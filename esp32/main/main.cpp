#inc    lude <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"


#include "test.h"

extern "C" {
	void app_main(void);
}

void app_main(void)
{

	int SR = 48000;
	int BS = 8;
	test test(SR, BS);
	test.start();

	while (1) {
		test.setParamValue("freq", rand() % (2000 - 50 + 1) + 50);
		vTaskDelay(1000 / portTICK_PERIOD_MS);
		printf(" setParamValue \n");
	}
}
